(function ($) {

Drupal.behaviors.urlFillVerticalTabs = {
  attach: function(context) {
    $('fieldset#edit-urlfill', context).drupalSetSummary(function (context) {
      var enabled = [];
      $('div.urlfill-wrapper', context).each(function(i, element) {
        var $element = $(element);
        if ($element.find('input.urlfill-onoff').is(':checked')) {
          enabled.push($element.data('field-name'));
        }
      });

      if (enabled.length > 0) {
        return Drupal.t('Fields enabled on @list', { '@list' : enabled.join(', ') });
      }
      else {
        return Drupal.t('URL Fill disabled');
      }
    });
  }
};

})(jQuery);
